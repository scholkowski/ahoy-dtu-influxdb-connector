import credentials
import datetime
import influxdb_client
import matplotlib.pyplot
import numpy


class InfluxDBConnector:
    def __init__(self, bucket: str, organisation: str, token: str, url: str, entity_id: str):
        self.bucket = bucket
        self.organisation = organisation
        self.token = token
        self.url = url
        self.entity_id = entity_id

    def query_data(self, start_date: datetime, stop_date: datetime) -> tuple:
        query = 'from(bucket: "' + self.bucket + '")\
                |> range(start: ' + start_date.isoformat() + 'Z, stop: ' + stop_date.isoformat() + 'Z)\
                |> filter(fn: (r) => r["entity_id"] == "' + self.entity_id + '")\
                |> filter(fn: (r) => r["_field"] == "value")'

        # Create client
        client = influxdb_client.InfluxDBClient(
            url=self.url,
            token=self.token,
            org=self.organisation
        )

        # Query data from database
        query_api = client.query_api()

        result = query_api.query(org=self.organisation, query=query)

        t_points = []
        y_points = []

        if result:
            for record in result[0].records:
                t_points.append(record.get_time())
                y_points.append(record.get_value())

        return t_points, y_points

    @staticmethod
    def get_time_span(days_back_start: int, days_back_stop: int) -> tuple:
        # Calculate time span
        today = datetime.date.today()
        target_start_date = today - datetime.timedelta(days=days_back_start)
        target_stop_date = today - datetime.timedelta(days=days_back_stop)

        start_date = datetime.datetime(year=target_start_date.year, month=target_start_date.month,
                                       day=target_start_date.day,
                                       hour=0, minute=0, second=0)

        stop_date = datetime.datetime(year=target_stop_date.year, month=target_stop_date.month,
                                      day=target_stop_date.day,
                                      hour=23, minute=59, second=59)

        return start_date, stop_date

    def get_energy_history(self, days_back_start: int, days_back_stop: int) -> list:
        energy_history = []

        # Get time span
        time_span = self.get_time_span(days_back_start=days_back_start, days_back_stop=days_back_stop)
        start_date = time_span[0]
        stop_date = time_span[1]

        # Sliding window over days
        current_date = start_date
        while current_date < stop_date:
            query_stop_date = current_date + datetime.timedelta(hours=23, minutes=59, seconds=59)

            result = self.query_data(current_date, query_stop_date)
            t_points = result[0]
            y_points = result[1]

            # Calculate time differences of t-axis in decimal hours and store them as absolute values
            counter = 0
            delta_t_abs = [0.0]
            previous_delta_t = 0.0
            while counter < t_points.__len__() - 1:
                delta_t = ((t_points[counter + 1] - t_points[counter]).total_seconds() / 3600)
                delta_t_abs.append(delta_t + previous_delta_t)
                previous_delta_t += delta_t
                counter += 1

            # Integrate y values over time to get energy in Wh
            energy = numpy.trapz(y_points, delta_t_abs)
            energy_history.append((current_date.strftime("%d-%m-%Y"), round(energy, 4), "Wh"))

            current_date += datetime.timedelta(days=1)

        return energy_history

    def get_power_history(self, days_back_start: int, days_back_stop: int) -> list:
        power_history = []

        # Get time span
        time_span = self.get_time_span(days_back_start=days_back_start, days_back_stop=days_back_stop)
        start_date = time_span[0]
        stop_date = time_span[1]

        # Sliding window over days
        current_date = start_date
        while current_date < stop_date:
            query_stop_date = current_date + datetime.timedelta(hours=23, minutes=59, seconds=59)

            result = self.query_data(current_date, query_stop_date)
            t_points = result[0]
            y_points = result[1]

            if t_points:
                power_history.append((t_points, y_points, "W"))
            else:
                power_history.append(([current_date], [0.0], "W"))

            current_date += datetime.timedelta(days=1)

        return power_history

    def plot_energy(self, days_back_start: int, days_back_stop: int):
        result = self.get_energy_history(days_back_start=days_back_start, days_back_stop=days_back_stop)

        x_values = []
        y_values = []
        for day in result:
            x_values.append(day[0])
            y_values.append(round(day[1]/1000, 2))

        params = {'axes.labelsize': 20, 'axes.titlesize': 20, 'legend.fontsize': 20, 'xtick.labelsize': 20,
                  'ytick.labelsize': 20}
        matplotlib.rcParams.update(params)
        matplotlib.pyplot.bar(x_values, y_values)
        matplotlib.pyplot.xlabel("Date")
        matplotlib.pyplot.ylabel("Energy [kWh]")
        matplotlib.pyplot.grid(color='black', linestyle='dotted', linewidth=1)
        matplotlib.pyplot.show()

    def plot_power(self, days_back_start: int, days_back_stop: int):
        result = self.get_power_history(days_back_start=days_back_start, days_back_stop=days_back_stop)

        x_values = []
        y_values = []

        for day in result:
            x_values += day[0]
            y_values += day[1]

        # Plot power over time
        params = {'axes.labelsize': 20, 'axes.titlesize': 20, 'legend.fontsize': 20, 'xtick.labelsize': 20,
                  'ytick.labelsize': 20}
        matplotlib.rcParams.update(params)
        matplotlib.pyplot.plot(x_values, y_values, '-')
        matplotlib.pyplot.xlabel("Day")
        matplotlib.pyplot.ylabel("Power [W]")
        matplotlib.pyplot.grid(color='black', linestyle='dotted', linewidth=1)
        matplotlib.pyplot.show()


if __name__ == '__main__':
    connector = InfluxDBConnector(bucket=credentials.bucket, organisation=credentials.org, token=credentials.token,
                                  url=credentials.url, entity_id=credentials.entity_id)

    connector.plot_energy(3, 0)
    connector.plot_power(3, 0)
    #eh = connector.get_energy_history(3, 0)
    #ph = connector.get_power_history(3, 0)
    pass
